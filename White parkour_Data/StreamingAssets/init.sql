DROP TABLE "setting";

CREATE TABLE "setting" (
	"id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	"graphics_id"	INTEGER NOT NULL,
	"antialiasing_id"	INTEGER NOT NULL,
	"v_sync_id"		INTEGER NOT NULL,
	"better_graphics_id"		INTEGER NOT NULL,
	"show_fps_id"		INTEGER NOT NULL,
	"custom_id"		INTEGER NOT NULL
);

CREATE TABLE "audio_settings" (
	"id"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	"audio_volume"	INTEGER NOT NULL,
);

INSERT INTO setting (graphics_id, antialiasing_id, v_sync_id, better_graphics_id, show_fps_id, custom_id) VALUES (0, 0, 0, 0, 0, 0);
INSERT INTO setting (graphics_id, antialiasing_id, v_sync_id, better_graphics_id, show_fps_id, custom_id) VALUES (1, 0, 0, 0, 0, 0);
INSERT INTO setting (graphics_id, antialiasing_id, v_sync_id, better_graphics_id, show_fps_id, custom_id) VALUES (2, 0, 1, 0, 0, 0);
INSERT INTO setting (graphics_id, antialiasing_id, v_sync_id, better_graphics_id, show_fps_id, custom_id) VALUES (3, 0, 1, 0, 0, 0);
INSERT INTO setting (graphics_id, antialiasing_id, v_sync_id, better_graphics_id, show_fps_id, custom_id) VALUES (4, 1, 1, 1, 1, 0);
INSERT INTO setting (graphics_id, antialiasing_id, v_sync_id, better_graphics_id, show_fps_id, custom_id) VALUES (5, 1, 1, 1, 1, 0);
INSERT INTO audio_settings (audio_volume) VALUES (1);